FROM tomee:8-jre-8.0.0-M2-webprofile

RUN apt update

RUN curl https://repo1.maven.org/maven2/org/postgresql/postgresql/42.2.9/postgresql-42.2.9.jar -o /usr/local/tomee/lib/postgresql-42.2.9.jar

COPY /target/SampleWebservice-1.0-SNAPSHOT.war /usr/local/tomee/webapps/SampleWebservice.war