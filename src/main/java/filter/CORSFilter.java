package filter;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/*
 * Because of the 'same-origin-policy' in browsers, we need enable CORS (cross-origin resource sharing).
 * We want to extend the JAX-RS runtime with that filter to permanently add the necessary "Access-Control-Allow-Origin"-Header,
 * this can be done with the annotation @Provider. Other possible solutions might fit as well for that problem eg. a proxy server
 * between frontend and backend.
 */
@Provider
public class CORSFilter implements ContainerResponseFilter {
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        responseContext.getHeaders().add("Access-Control-Allow-Origin","*");
        responseContext.getHeaders().add("Access-Control-Allow-Headers", "Content-Type");
        // only supported for Chrome, Firefox, Opera 
        responseContext.getHeaders().add("Access-Control-Allow-Methods", "*");
    }
}
