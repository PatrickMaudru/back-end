package exception;

import javax.json.stream.JsonParsingException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/*
 * Exception mapper to handle exceptions that might occur mapping JSON to Object (eg. in POST method)
 * We just want to deliver an appropriate status code to our client instead of code 500
 */
@Provider
public class JSONparseExceptionMapper implements ExceptionMapper<JsonParsingException> {

    @Override
    public Response toResponse(JsonParsingException exception) {
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
