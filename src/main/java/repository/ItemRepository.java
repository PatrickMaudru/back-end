package repository;

import entities.Item;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

public class ItemRepository {
    @PersistenceContext(unitName = "item")
    private EntityManager entityManager;

    @Transactional
    public void persistItem(Item item) {
        entityManager.persist(item);

    }


    public List<Item> findAllItems() {
        Query query = entityManager.createQuery("SELECT t FROM Item t");
        return query.getResultList();
    }

    @Transactional
    public void changeItem(Item changedItem) {
        entityManager.merge(changedItem);

    }

    @Transactional
    public void deleteItem(Item itemToBeDeletetd) {
        Item databaseItem = findOneItem(itemToBeDeletetd.getId());
        entityManager.remove(databaseItem);
    }

    public Item findOneItem(Integer id) {
        return entityManager.find(Item.class, id);
    }

}
