package controller;

import entities.Item;
import service.ItemService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Path("/api/item") // We define a Basepath
@Produces(MediaType.APPLICATION_JSON) // We can define a global MediaType or per Method with @Produces
public class Controller {

    // creating a Logger is always a good thing
    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    @Inject
    private ItemService itemService;

    @GET
    @Path("/all")
    public List<Item> getAll(){
        return itemService.getAllItems();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateItem(@Valid Item item){
        LOGGER.info("Updating Item: "+item);
        Boolean success = itemService.updateItem(item);
        Response.Status response = success ? Response.Status.OK : Response.Status.BAD_REQUEST;
        return Response.status(response).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteItem(@Valid Item item){
        LOGGER.info("Deleting Item: "+item);
        Boolean success = itemService.deleteItem(item);
        Response.Status response = success ? Response.Status.OK : Response.Status.BAD_REQUEST;
        return Response.status(response).build();
    }

    /*
     *    POST-Method URL: http://localhost:8080/api/item
     *    We are accepting JSON as in the @Consumes Annotation defined. The JSON Body is mapped to Item Entity.
     *    @Valid ensures that all properties in Item that are marked with @NotNullable are not null
     *    Since save-method in our service returns true or false, we'll send an appropriate response code to our clients
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createItem(@Valid Item item){
        LOGGER.info("Creating Item: "+item);
        Boolean success = itemService.saveItem(item);
        Response.Status response = success ? Response.Status.OK : Response.Status.BAD_REQUEST;
        return Response.status(response).build();
    }

}
