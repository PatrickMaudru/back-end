package service;

import entities.Item;
import repository.ItemRepository;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

/*
 * Kind of middleware between our REST-Controller and repository. We encapsulate repository access to be able to
 * handle various exception that might occure. We don't want to deliver HTTP-status 500 and stacktraces to our clients.
 */
public class ItemService {

    private static final Logger LOGGER = Logger.getLogger(ItemService.class.getName());

    /*
     * We want CDI (TomcatEE) to take care about object instantiation
     */
    @Inject
    private ItemRepository itemRepository;

    /*
     * For all methods here we are accessing our DB via repository, we catch all eventual exceptions that might occur
     */
    public Boolean saveItem(Item item){
        try {
            itemRepository.persistItem(item);
            return true;
        }catch (Exception e){
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    public Boolean deleteItem(Item item) {
        try {
            itemRepository.deleteItem(item);
            return true;
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    public Boolean updateItem(Item item) {
        try {
            itemRepository.changeItem(item);
            return true;
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    public List<Item> getAllItems() {
        return itemRepository.findAllItems();
    }
}
